FROM tomcat:9.0
COPY test-project-0.1.war.original /usr/local/tomcat/webapps/ROOT.war
EXPOSE 8080
CMD ["catalina.sh", "run"]
